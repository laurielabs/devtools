# Grab png file from kinect streaming to copy to your local
# Set up your ssh key to allow password-less ssh to llabs
# You can add your ~/.ssh/id_rsa.pub to ~/.ssh/authorized_keys on llabs
import os

for i in xrange(1,15):
    remote_command = "ssh -t llabs@10.0.1.39 \"echo Laurie95 | sudo -S python /home/llabs/llabs/tools/yinxie/tool.py >/home/llabs/llabs/tools/yinxie/output.txt\" \r\n"
    print "Execute"
    os.system(remote_command)
    print "SCP"
    scp_command = "scp llabs@10.0.1.39:/home/llabs/llabs/tools/yinxie/output.png ."
    os.system(scp_command)
    os.rename('output.png', '{}.png'.format(i))
    os.system('open {}.png'.format(i))
    raw_input("Press Enter to continue...")
